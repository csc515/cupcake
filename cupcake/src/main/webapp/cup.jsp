<!doctype html>
<%@page import="java.util.Date"%>
<html lang="en">
<head>
<title>Yummy</title>
<style>
	html, body {
		height: 100%;
		body: 100%;
		margin:0;
		padding:0;
	}
	#footer {
		position: absolute;
		bottom: 0;
		width: calc(100% - 8px);
		height: 60px; /* Height of the footer */
		text-align: center;
	}
	.cupcakeimage {
		height: 100%;
		/* width: 100%; */
	}
</style>
</head>
<body>
	<img class="cupcakeimage" alt="yummy cupcake" src="<%=request.getContextPath()%>/resources/img/cupcake.png" />
	<div id="footer">
		Boss Cakes &copy;<%=new Date().getYear() %> 
	</div>
</body>
</html>